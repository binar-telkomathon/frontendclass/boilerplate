import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const ProtectedRoute = ({ isAuth, component: Component, ...rest }) => {
    console.log('isAuth =>', isAuth);
    // const { isAuth, component: Component, ...rest } = props;
    return (
        <Route
            {...rest}
            render={routeProps => {
                if(isAuth) {
                    return <Component {...routeProps} />
                } else {
                    return (
                        <Redirect to="/" />
                    )
                }
            }}
        />
    );
};

const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuthenticated
});

export default connect(mapStateToProps)(ProtectedRoute);