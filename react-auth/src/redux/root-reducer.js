import { combineReducers } from 'redux';
import counterReducer from './counter/reducers';
import authReducer from './authentication/reducers';

const rootReducer = {
    counter: counterReducer,
    auth: authReducer
};

export default combineReducers(rootReducer);