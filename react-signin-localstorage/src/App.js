import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import ProtectedRoute from './components/ProtectedRoute';
import AboutMe from './pages/AboutMe';
import Counter from './pages/Counter';
import Home from './pages/Home';
import Navbar from './components/Navbar';
import Hooks from './pages/Hooks';
import Contributor from './pages/Contributor';
import Buku from './pages/Buku';
import Login from './pages/Login';
import SignIn from './pages/SignIn';
import { logInAction } from './redux/authentication/actions';


function App(props) {
  const { logInFunc } = props;

  useEffect(() => {
    if(localStorage.getItem('token')) {
        logInFunc();
    }
}, [logInFunc]);

  return (
    <BrowserRouter>
      <Navbar />
      <Switch>

        <Route exact path="/" component={Home} />
        <ProtectedRoute path="/about-me" component={AboutMe} />
        <Route path="/counter" component={Counter} />
        <Route path="/hooks" component={Hooks} />
        <Route path="/contributor" component={Contributor} />
        <Route path="/buku" component={Buku} />
        <Route path="/login" component={Login} />
        <Route path="/signin" component={SignIn} />

      </Switch>
    </BrowserRouter>
  );
}

const mapDispatchToProps = (dispatch) => ({
  logInFunc: () => dispatch(logInAction),
});

export default connect(null, mapDispatchToProps)(App);