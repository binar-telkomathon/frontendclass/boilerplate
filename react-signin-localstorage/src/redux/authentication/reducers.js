import { LOG_IN, LOG_OUT } from './actionTypes';

const initialState = {
    isAuthenticated: false,
};

const authReducer = (state = initialState, action) => {
    if (action.type === LOG_IN) {
        return {
            ...state,
            isAuthenticated: true
        }
    } else if (action.type === LOG_OUT) {
        return {
            ...state,
            isAuthenticated: false,
        }
    }
    return state;
};

export default authReducer;