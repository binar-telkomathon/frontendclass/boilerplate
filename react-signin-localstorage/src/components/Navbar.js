import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import '../assets/css/navbar.css';
// import {
//     Collapse,
//     Navbar as RNavbar,
//     NavbarToggler,
//     NavbarBrand,
//     Nav,
//     NavItem,
    // NavLink,
//     UncontrolledDropdown,
//     DropdownToggle,
//     DropdownMenu,
//     DropdownItem,
//     NavbarText
//   } from 'reactstrap';

const Navbar = () => {
    // const [isOpen, setIsOpen] = useState(false);
    // const toggle = () => setIsOpen(!isOpen);

    return (
        <div className="link-nav">
            
            {/* <RNavbar color="light" light expand="md">
                <NavbarBrand href="/">Jennie Diary Blog</NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                <Nav className="mr-auto" navbar>
                    <NavItem>
                        <NavLink>
                            <Link className="navbar-link" to="/">Home</Link>
                        </NavLink>
                    </NavItem>
                    <NavItem>
                    <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
                    </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                        Options
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem>
                        Option 1
                        </DropdownItem>
                        <DropdownItem>
                        Option 2
                        </DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem>
                        Reset
                        </DropdownItem>
                    </DropdownMenu>
                    </UncontrolledDropdown>
                </Nav>
                <NavbarText>Simple Text</NavbarText>
                </Collapse>
            </RNavbar> */}
            <Link to="/"><p className="link-nav">Home</p></Link> | 
            <Link to="/about-me">About Me</Link> |
            <Link to="/counter">Counter</Link> | 
            <Link to="/hooks">Hooks</Link> |
            <Link to="/contributor">Contributor</Link> |
            <Link to="/buku">Buku</Link> | 
            <Link to="/login">Login</Link>
        </div>
    );
};

export default Navbar;