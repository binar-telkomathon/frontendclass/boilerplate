import React, { useState } from 'react';

const Counter = () => {
    console.log('tes');
  const [click, setClick] = useState(100);
  const [nama, setNama] = useState('Bambang');

  // function klikGan() {
  //   alert('tes yuk');
  // }

  const klikGan = () => {
    alert('tes yuk yuk yuk');
  };

  function klikGanJuga() {
    alert('tes klik gan juga');
  };

    return (
        <div className="App">
            <div className="judul">Tes 123</div>
            <button onClick={klikGan}>Yuk Klik</button>

            <div>Counter App</div>
            <p>Anda ngeklik {click} kali</p>
            <button onClick={() => setClick(click + 2)}>Klik +</button>
            <button onClick={() => setClick(click - 2)}>Klik -</button>


            <div>------------------</div>
            {/* <Card nama={nama} /> */}
            <p>Ini Kartu</p>
            <p>{nama}</p>
            <button onClick={() => setNama('mas Rahadian')}>Ubah nama!</button>
        </div>
    )
}

export default Counter;