import React, { useState } from 'react';
import imgBook from '../assets/images/book.jpg';
import CardBook from '../components/CardBook';

const Buku = () => {
    const [data, setData] = useState([]);

    const handleAdd = () => {
        const newData = {
            id: data.length + 1,
            name: `Buku ${data.length + 1}`,
            img: imgBook,
        }
        const newArr = [...data];
        // const result = newArr.concat(newData); // using es5
        const result = [...newArr, newData]; // using es6

        // const result = data.push(newData);
        setData(result);
    };

    const handleRemove = () => {
        const newArr = [...data];
        const result = newArr.slice(0, -1);
        // const result = newArr.pop();
        setData(result);
    }

    // console.log('data =>', data);

    return (
        <>
            <p>Buku</p>
            <button onClick={handleAdd}>Add Book</button>
            <button onClick={handleRemove}>Remove Book</button>
            {data.map((list, index) => (
                <ul key={index}>
                    <li>
                        <CardBook
                            name={list.name}
                            // id={list.id}
                            img={list.img}
                        />
                    </li>
                </ul>
            ))}
        </>
    )
}

export default Buku;