import React, { useState } from 'react';
import { Button } from 'reactstrap';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import qs from 'qs';
import axios from 'axios';
import { logInAction, logOutAction } from '../redux/authentication/actions';

const Login = (props) => {
    const { isLogin, logInFunc, logOutFunc } = props;
    const history = useHistory();
    const [regData, setRegData] = useState({
        email: '',
        password: ''
    });
    // console.log('props login page =>', props);

    const handleClick = () => {
        if (isLogin) {
            logOutFunc();
            history.push('/');
        } else {
            logInFunc();
            history.push('/counter');
        }
    }

    const handleChangeReg = (e) => {
        setRegData({
            ...regData,
            [e.target.name] : e.target.value
            // jadinya
            // password: yguserketik
            // email: yguserketik
        })
    }

    const handleSubmitReg = (e) => {
        e.preventDefault();
        axios({
            method: 'post',
            url: 'https://reqres.in/api/register',
            data: qs.stringify({
              email: regData.email,
              password: regData.password,
            }),
            headers: {
             'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
          }).then((response) => {
            console.log('response =>', response);
          })
    }

    console.log('regData =>', regData);
    return (
        <>
            <Button onClick={handleClick} color="info">{isLogin ? 'Logout' : 'Login'}</Button>

            <div>--------------------------------------------</div>
            <h1>Register</h1>
            <form>
                <p>Email</p>
                <input
                    type="email"
                    placeholder="Ketik emailmu di sini ..."
                    name="email"
                    onChange={handleChangeReg}
                />
                
                <p style={{ marginTop: '20px' }}>Password</p>
                <input
                    type="password"
                    placeholder="Ketik passwordmu di sini ..."
                    name="password"
                    onChange={handleChangeReg}
                />
                <br />
                <button color="warning" onClick={handleSubmitReg} type="submit">Daftar</button>
            </form>
        </>
    );
};

const mapStateToProps = (state) => ({
    isLogin: state.auth.isAuthenticated
});

const mapDispatchToProps = (dispatch) => ({
    logInFunc: () => dispatch(logInAction),
    logOutFunc: () => dispatch(logOutAction)
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);