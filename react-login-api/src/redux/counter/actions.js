import { HANDLE_ADD, HANDLE_MINUS } from './actionTypes';

export const handleAddAction = {
    type: HANDLE_ADD
};

export const handleMinusAction = {
    type: HANDLE_MINUS
};