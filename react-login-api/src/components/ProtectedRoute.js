import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const ProtectedRoute = ({ isAuth, component: Component, ...rest }) => {
    console.log('isAuth =>', isAuth);
    console.log('...rest =>', rest);
    // const { isAuth, component: Component, ...rest } = props;
    return (
        <Route
            {...rest}
            render={routeProps => {
                console.log('routeProps =>', routeProps);
                if(isAuth) {
                    return <Component {...routeProps} />
                } else {
                    return (
                        <Redirect to="/" />
                    )
                }
            }}
        />
    );
};

const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuthenticated
});

export default connect(mapStateToProps)(ProtectedRoute);