import './App.css';
import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import AboutMe from './pages/AboutMe';
import Counter from './pages/Counter';
import Home from './pages/Home';
import Navbar from './components/Navbar';

function App() {

  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/about-me">
          <AboutMe />
        </Route>
        <Route path="/counter">
          <Counter />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
