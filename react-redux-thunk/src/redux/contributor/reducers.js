import { GET_CONTRI_ERROR, GET_CONTRI_REQUEST, GET_CONTRI_SUCCESS} from './actionTypes';

const initialState = {
    data: [],
    loading: null,
    error: null,
};

const contriReducer = (state = initialState, action) => {
    if (action.type === GET_CONTRI_REQUEST) {
        return {
            ...state,
            loading: action.payload,
        } 
    } else if (action.type === GET_CONTRI_SUCCESS) {
        return {
            ...state,
            data: action.data,
        }
    } else if (action.type === GET_CONTRI_ERROR) {
        return {
            ...state,
            error: action.error,
        }
    }
    return state;
}

export default contriReducer;