import axios from 'axios';
import { GET_CONTRI_ERROR, GET_CONTRI_REQUEST, GET_CONTRI_SUCCESS} from './actionTypes';

const getContriReq = (payload) => {
    return {
        type: GET_CONTRI_REQUEST,
        payload,
    };
};

const getContriSuccess = (data) => {
    return {
        type: GET_CONTRI_SUCCESS,
        data,
    };
};

const getContriErr = (error) => {
    return {
        type: GET_CONTRI_ERROR,
        error,
    };
};

export const getContriData = () => (dispatch) => {
    // dispatch({type: GET_CONTRI_REQUEST, payload: true});
    dispatch(getContriReq(true));
    axios.get('https://reqres.in/api/users?page=1').then((res) => {
        console.log('res contri =>', res);
        const data = res.data.data;
        dispatch(getContriSuccess(data));
        dispatch(getContriReq(false));
    }).catch((err) => {
        console.log('error contri =>', err);
        dispatch(getContriErr(err));
        dispatch(getContriReq(false));
    })
};