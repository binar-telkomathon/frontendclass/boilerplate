import { LOG_IN, LOG_OUT } from './actionTypes';

// export const logInAction = (payload) => {
//     console.log('payload =>', payload);
//     return {
//         type: LOG_IN,
//         payload,
//     }
// };

// export const logInAction2 = (payload) => ({
//     type: LOG_IN,
//     payload,
// });

// export const loginActionOld = {
//     type: LOG_IN,
// }

export const logInAction = (data) => {
    return {
        type: LOG_IN,
        data,
    }
};

export const logOutAction = {
    type: LOG_OUT
};