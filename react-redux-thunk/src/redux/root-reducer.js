import { combineReducers } from 'redux';
import counterReducer from './counter/reducers';
import authReducer from './authentication/reducers';
import contriReducer from './contributor/reducers';

const rootReducer = {
    counter: counterReducer,
    auth: authReducer,
    contri: contriReducer,
};

export default combineReducers(rootReducer);