// import { HANDLE_ADD, HANDLE_MINUS } from './actionTypes';
const HANDLE_ADD = 'HANDLE_ADD';
const HANDLE_MINUS = 'HANDLE_MINUS';

export const handleAddAction = {
    type: HANDLE_ADD
};

export const handleMinusAction = {
    type: HANDLE_MINUS
};