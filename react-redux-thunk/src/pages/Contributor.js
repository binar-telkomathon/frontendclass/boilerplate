import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux'; // ini adalah hooks dari react redux, pengganti connect
import { Spinner } from 'reactstrap';
import { getContriData } from '../redux/contributor/actions';

const Contributor = () => {
    const dispatch = useDispatch();

    const contriData = useSelector((states) => states.contri.data);
    const isAuth = useSelector((states) => states.auth.isAuthenticated);
    const loading = useSelector((states) => states.contri.loading);
    const error = useSelector((states) => states.contri.error);

    const [users, setUsers] = useState([]); // set initialStatenya, dari bentuk data yang mau kita ambil dari BE
    const feAthon = [
        {
            nama: "Aaron",
            email: "aaron@gmail.com",
        },
        {
            nama: "Bambang",
            email: "bambang@google.com",
        },
    ];

    useEffect(() => {
        // taruh kode ngambil data (fetching Data) dari BE di sini
        // di sinilah, terjadi proses componentDidMount dan componenttDidUpdate
        const fetchData = async () => {
            const result = await axios('https://reqres.in/api/users?page=2');
            setUsers(result.data.data); // data spesifik yang kita mau tampilkan di FE, ada di sini
        };
        fetchData();
        dispatch(getContriData());
    }, [dispatch]); // array kosong ini, adalah willUnmount kalau dibandingan dengan class component

    console.log('users =>', users);
    console.log('contriData =>', contriData);
    console.log('isAuth contri =>', isAuth);
    console.log('loading =>', loading);
    console.log('error =>', error);

    return (
        <>
            <h1>Big thanks to:</h1>

            {users.map((list, index) => (
                <ul key={index}>
                    <li>{list.email}</li>
                    <li>{list.first_name}</li>
                    <img alt="gambar" src={list.avatar} />
                </ul>
            ))};

            {feAthon.map((element, key) => {
                console.log('element =>', element);
                console.log('key =>', key);
                return (
                    <div key={key}>
                        <p>{element.nama}</p>
                    </div>
                )
            })}

            <br /><br />
            <div>--------------------------------</div>
            {loading ? <Spinner color="primary" /> : contriData.map((list, key) => (
                <div key={key}>
                    <p>{list.email}</p>
                    <img src={list.avatar} alt="avatar" />
                </div>
            ))}
        </>
    );
};

export default Contributor;