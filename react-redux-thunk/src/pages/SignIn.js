import React, { useState } from 'react';
import axios from 'axios';
import qs from 'qs';

const SignIn = () => {

    const [signInData, setSignInData] = useState({
        email: '',
        password: '',
    });

    const handleChangeSignIn = (e) => {
        setSignInData({
            ...signInData,
            [e.target.name]: e.target.value,
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        axios({
            method: 'post',
            url: 'https://reqres.in/api/login',
            data: qs.stringify({
              email: signInData.email,
              password: signInData.password,
            }),
            headers: {
             'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
          }).then((response) => {
            console.log('response =>', response);
            localStorage.setItem('token', response.data.token);
          }).catch((error) => {
              console.log(error);
          })
        console.log('submit');
    };

    return (
        <>
            <form>
                <p>Email</p>
                <input
                    type="email"
                    placeholder="Type yor email"
                    name="email"
                    onChange={handleChangeSignIn}
                />

                <p>Password</p>
                <input
                    type="password"
                    placeholder="Type yor pass"
                    name="password"
                    onChange={handleChangeSignIn}
                />


                <button onClick={handleSubmit} type="submit">SignIn</button>
            </form>
        </>
    );
};


export default SignIn;